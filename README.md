
## LC学Go

> GOPATH 下建三个文件夹 bin pkg src.

注意：一定要将你的项目放在GOPATH的src目录下 例如本项目【godemobylc】在 GOPATH的src就是你自己的项目所在文件夹
> D:\all_develop_soft\go1.13.windows-amd64\go_path\src\godemobylc

注意:今天使用 IDEA 打算debug此项目go文件main方法的时候遇到这么一个问题：
   
> runnerw.exe: CreateProcess failed with error 2

原因：此情况产生的原因是IDEA版本ideaIU-2018.1版本太低，安装的Go语言的插件都很低,故换成最新版本的IDEA即可解决此问题。

IDEA版本(2019.1.2)+Go语言(1.12.5)最新版本,经测试可以debug了。

IDEA最新版本ideaIU-2019.1.3如下所示:

Version: 2019.1.3

Build: 191.7479.19

Released: May 28, 2019

> 当前时间：2019年6月10日15:11:07

### 其他配置相关以及网站
将gobin和将gopath自定义路径下的bin添加的环境变量-系统变量-Path  
> D:\all_develop_soft\go1.13.windows-amd64\go\bin

>  D:\all_develop_soft\go1.13.windows-amd64\go_path\bin

默认gopath路径:  
> C:\Users\LC\go

将goroot(GOROOT)自定义路径添加到环境变量-系统变量  
> D:\all_develop_soft\go1.13.windows-amd64\go

将gopath(GOPATH)自定义路径添加到环境变量-系统变量  
> D:\all_develop_soft\go1.13.windows-amd64\go_path

使用gopm下载安装go语言包 - chenzhigang - 博客园  
> https://www.cnblogs.com/kimkat/p/9938620.html

```
使用gopm代替go下载

由于国内的网络环境，go get能够下载github上的包，但是不能下载golang.org上的包。因此，使用第三方工具gopm来下载管理包。

https://gopm.io/

下载安装gopm的命令:go get -u github.com/gpmgo/gopm ,安装后，会在$GOPATH/bin中放入gopm.exe文件。
使用gopm下载安装包，默认只下载到仓库，因此要加-g参数。 命令行为:  gopm get -g -u -v golang.org/x/text
 
```

Go 语言包管理 - 版本化缓存和分发 Go 语言包 gopm官方网站  
> https://gopm.io/

gopm 下载存放缓存目录  
> C:\Users\LC\.gopm\repos

解决go get无法下载被墙的包 - 简书  
> https://www.jianshu.com/p/81fa51c4125e

golang 在windows中设置环境变量 - keepd的博客 - CSDN博客  
> https://blog.csdn.net/keepd/article/details/79430254

Go语言标准库文档中文版 | Go语言中文网 | Golang中文社区 | Golang中国  
> https://studygolang.com/pkgdoc

yinggaozhen/awesome-go-cn: 一个很棒的Go框架、库和软件的中文收录大全。 脚本定期与英文文档同步，包含了各工程star数/最近更新时间，助您快速发现优质项目。Awesome Go~  
> https://github.com/yinggaozhen/awesome-go-cn

disintegration/imaging: Imaging is a simple image processing package for Go  
> https://github.com/disintegration/imaging

golang/net: [mirror] Go supplementary network libraries  使用镜像仓库  
> https://github.com/golang/net
```
cd D:\all_develop_soft\go1.13.windows-amd64\go_path\src\golang.org\x

git clone https://github.com/golang/net.git

即可下载
```
Go 镜像库其他还有更多  直接使用git clone 即可下载  
> https://github.com/golang

GoDoc  需科学上网  
> https://godoc.org/

net - GoDoc  
https://godoc.org/golang.org/x/net

Golang读写文件的几种方式 - 简书  
> https://www.jianshu.com/p/7790ca1bc8f6

### go install 成功了  
```
命令：
PS D:\all_develop_soft\go1.13.windows-amd64\go_path\src> go install godemobylc/com/lc/pkg
PS D:\all_develop_soft\go1.13.windows-amd64\go_path\src>
```
```
成功之后，在 
D:\all_develop_soft\go1.13.windows-amd64\go_path\pkg\windows_amd64\godemobylc\com\lc
目录下会有一个pkg.a的文件
```

永远不要放弃你的梦想！~LC

 - [LC博客-一加壹博客最Top](http://www.oneplusone.vip)
 - [LC · GitLab](https://gitlab.com/ahviplc)
 - [LC · goDemoByLC · GitLab](https://gitlab.com/ahviplc/godemobylc)
 
### LC最寄语:多想想明天的你！~LC

> LC 2018-12-24 14:54:31

update time  
> 2019-9-14 12:58:50