package utils

import "fmt"

func MyTest2() {
	fmt.Println(" i am from com/lc/utils/utils2.go begin------------------------------------------")
	UtilsApp()
	fmt.Println(" i am from com/lc/utils/utils2.go end------------------------------------------")
}

func init() {
	fmt.Println("【3】 com/lc/utils/utils2 下的 init函数，用于初始化一些信息。。。")
}
