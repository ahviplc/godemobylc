package utils

import (
	"fmt"
	"log"
)

func UtilsApp() {
	fmt.Println("i am utilsApp from com/lc/utils/utils.go:5")
}

//有多个init函数的时候，谁先写就先调用谁
func init() {
	fmt.Println("【1】com/lc/utils/utils.go 下的 init函数，用于初始化一些信息。。。")
}

func init() {
	fmt.Println("【2】另一个 com/lc/utils/utils.go 下的 init函数，用于初始化一些信息。。。")
}

//处理err的函数
func HandleErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
