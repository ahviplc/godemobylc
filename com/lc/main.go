package main

//import "godemobylc/com/lc/go/utilsByLC" 绝对路径
//import "godemobylc/com/lc/utils"
//import "godemobylc/com/lc/utils/uuidutils"

import (
	"fmt"
	"godemobylc/com/lc/go/utilsByLC"
	p "godemobylc/com/lc/person"
	"godemobylc/com/lc/pkg"
	"godemobylc/com/lc/utils"
	"godemobylc/com/lc/utils/uuidutils"
)

func main() {
	/**
	   关于包的使用:
	  1:一个包的统计文件归属一个包。package的声明要一致
	  2：package声明的包和对应的目录可以不一致，但是习惯上还是写成一致的
	  3:包可以嵌套
	  4：同包下的函数不需要导入包，可以直接使用
	  5:main包，main()函数所在的包，其他包不能使用
	  6:导入包的使用，路径要从src下开始写【src是GOPATH下的src,GOPATH也就是自己的所有项目所在的目录】

	*/

	//fmt.Printf("LC-main")
	fmt.Println("LC-main")

	fmt.Println("------------------------------------------")

	//调用自定义函数
	//fmt.Printf(utilsByLC.GetMd5("1"))
	fmt.Println(utilsByLC.GetMd5("admin"))

	utilsByLC.PrintNowTime()

	utils.UtilsApp()

	fmt.Println("------------------------------------------")

	uuidutils.UuidApp()

	pkg.MyTest()

	utils.MyTest2()

	fmt.Println("------------------------------------------")

	p1 := p.Person{"LC", 18, "男"}                  //p是包起的别名
	p2 := p.Person{Name: "LC2", Age: 18, Sex: "男"} //加上key
	fmt.Println(p1.Name, p1.Age, p1.Sex)
	fmt.Println(p2.Name, p2.Age, p2.Sex)
}
