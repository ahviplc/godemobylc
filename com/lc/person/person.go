package person

//Person首字母大写 代表是公共的 可以被其他包导入 访问
//对象下定义的字段首字母大写 也是这个意思 如果字段的首字母小写 则是私有 不可见 不可访问的
type Person struct {
	Name string
	Age  int
	Sex  string
}
