// utilsByLC/md5Utils.go
package utilsByLC

import (
	"crypto/md5"
	"fmt"
	"io"
	"time"
)

func GetMd5(str string) string {
	h := md5.New()
	io.WriteString(h, str)
	return fmt.Sprintf("%x", h.Sum(nil))
}

func PrintNowTime() {
	fmt.Println(time.Now())
}

//func main() {
//	fmt.Println(GetMd5("admin"))
//}
