package main

import "fmt"

/*
 Author by LC
 2018-12-24 12:55:32
*/

//go build -o hello.linux hello_world.go   #编译成linux系统可以执行的二级制程序
//go build -o hello.exe hello_world.go   #编译成windows系统可以执行的二进制程序
//go build -o hello.mac hello_world.go   #编译成mac系统可以执行的二进制程序
//gofmt hello_world.go  #在看看格式化后的输出，是不是一下就改变了三观，哈哈！

var a = "一加壹博客最Top"
var b string = "oneplusone.com"
var c bool

func main() {
	fmt.Println("Hello, World!GO LC！~LC")
	println(a, b, c)

	var aa string = "abc"
	fmt.Println("hello, world", aa)

}
