package main

import (
	"fmt"
	"log"
	"net/url"
)

func main() {
	u, err := url.Parse("http://bing.com/search?q=dotnet")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(u)
	fmt.Println(u.Scheme)
	fmt.Println(u.Opaque)
	fmt.Println(u.Host)
	fmt.Println(u.Path)
	fmt.Println(u.RawPath)
	fmt.Println(u.ForceQuery)
	fmt.Println(u.RawQuery)
	fmt.Println(u.Fragment)

	fmt.Println("------------------------------------------------------------------------------------------------")
	values, err2 := url.ParseRequestURI("https://www.baidu.com/s?wd=%E6%90%9C%E7%B4%A2&rsv_spt=1&issp=1&f=8&rsv_bp=0&rsv_idx=2&ie=utf-8&tn=baiduhome_pg&rsv_enter=1&rsv_sug3=7&rsv_sug1=6")
	fmt.Println(values)
	if err2 != nil {
		fmt.Println(err2)
	}
	urlParam := values.RawQuery
	fmt.Println(urlParam)

	fmt.Println("------------------------------------------------------------------------------------------------")


}
