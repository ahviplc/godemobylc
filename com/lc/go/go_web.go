/*
 简单的web界面
 Author by LC
 2018-12-24 12:59:08
*/

package main

import (
	"fmt"
	"net/http"
)

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "Welcome to LC's home page!")
}

func main() {
	http.HandleFunc("/", handler)
	http.ListenAndServe(":8087", nil)

	fmt.Println("请打开localhost:8087，进行浏览！")
}
