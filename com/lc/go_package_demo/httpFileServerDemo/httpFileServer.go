package main

import (
	"fmt"
	"net/http"
)

/*
  文件服务器
*/

func main() {
	http.Handle("/", http.FileServer(http.Dir(".")))
	fmt.Println("http File Server is Starting ip:8090 ...")
	http.ListenAndServe(":8090", nil)

}
