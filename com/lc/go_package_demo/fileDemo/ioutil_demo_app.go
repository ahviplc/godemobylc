package main

import (
	"fmt"
	"io/ioutil"
	"os"
)

func main() {

	/*
			   iouti包
					ReadFile()
					WriteFile()
		            ReadAll()
					ReadDir()
	*/

	////1.读取文件中的所有的数据
	//fileName := "com/lc/go_package_demo/fileDemo/ioTxt.txt"
	//data,err := ioutil.ReadFile(fileName)
	//fmt.Println(err) //<nil>
	//fmt.Println(data) //[97 98 99 100 101 102 103 104 105 106 65 66 67]
	//fmt.Println(string(data)) //abcdefghijABC

	////2。写出数据
	//fileName := "com/lc/go_package_demo/fileDemo/ioTxt_ioutil.txt"
	////s1 := "helloworld面朝大海，春暖花开"
	//s1 := "@helloworld面朝大海，春暖花开@"
	//err := ioutil.WriteFile(fileName,[]byte(s1),os.ModePerm)
	//fmt.Println(err) //<nil>

	////3.ReadAll()
	//s2 := "i am LC"
	//r1 := strings.NewReader(s2)
	//data, err := ioutil.ReadAll(r1)
	//fmt.Println(err)          //<nil>
	//fmt.Println(data)         //[105 32 97 109 32 76 67]
	//fmt.Println(string(data)) //i am LC

	////4.ReadDir() 读取一个目录下的字内容:子文件和子目录，但是只能读取一层
	//dirName := "com/lc/go_package_demo/fileDemo"
	//fileInfo, err := ioutil.ReadDir(dirName)
	//if err != nil {
	//	fmt.Println("err", err) //err
	//	return
	//}
	//fmt.Println(len(fileInfo))
	//
	////循环遍历 其里面的内容
	//for i := 0; i < len(fileInfo); i++ {
	//	//fmt.Printf("%T\n", fileInfo[i]) //文件类型
	//	fmt.Printf("第%d个：名称：%s，是否是目录：%t\n", i, fileInfo[i].Name(), fileInfo[i].IsDir())
	//}

	//第0个：名称：FileDemoApp.go，是否是目录：false
	//第1个：名称：LC_Life_Condiment_Copy.jpg，是否是目录：false
	//第2个：名称：LC_Life_Condiment_Copy2.jpg，是否是目录：false
	//第3个：名称：LC_Life_Condiment_Copy3.jpg，是否是目录：false
	//第4个：名称：bufioTxt.txt，是否是目录：false
	//第5个：名称：bufioTxt_w.txt，是否是目录：false
	//第6个：名称：bufio_r_demo_app.go，是否是目录：false
	//第7个：名称：bufio_w_demo_app.go，是否是目录：false
	//第8个：名称：fileCopyDemoApp.go，是否是目录：false
	//第9个：名称：fileInfoDemoApp.go，是否是目录：false
	//第10个：名称：fileSeekDemoApp.go，是否是目录：false
	//第11个：名称：fileSeekDemoApp2.go，是否是目录：false
	//第12个：名称：ioReadDemoApp.go，是否是目录：false
	//第13个：名称：ioTxt.txt，是否是目录：false
	//第14个：名称：ioTxtWrite.txt，是否是目录：false
	//第15个：名称：ioTxtWrite_Copy.txt，是否是目录：false
	//第16个：名称：ioTxt_ioutil.txt，是否是目录：false
	//第17个：名称：ioWriteDemoApp.go，是否是目录：false
	//第18个：名称：ioutil_demo_app.go，是否是目录：false
	//第19个：名称：lcTxt.txt，是否是目录：false
	//第20个：名称：lcTxt_diy.txt，是否是目录：false
	//第21个：名称：mkdir_diy，是否是目录：true

	//5.临时目录和临时文件
	dir, err := ioutil.TempDir("com/lc/go_package_demo/fileDemo", "Test")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer os.RemoveAll(dir)
	fmt.Println(dir)

	//创建临时文件
	file, err := ioutil.TempFile(dir, "Test")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer file.Close() //先关闭 再删除
	defer os.RemoveAll(file.Name())
	fmt.Println(file.Name())

	//com\lc\go_package_demo\fileDemo\Test633277271
	//com\lc\go_package_demo\fileDemo\Test633277271\Test003926730
	//执行了 但是没有 说明生成了 没有使用 又删除了
}
