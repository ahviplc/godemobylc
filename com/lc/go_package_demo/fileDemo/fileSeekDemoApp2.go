package main

import (
	"fmt"
	"godemobylc/com/lc/utils"
	"io"
	"os"
	"strconv"
	"strings"
)

func main() {

	/*
				  Seek(offset int64, whence int) (ret int64, err error),设置指针光标的位置
				     第一个参数:偏移量
				     第二个参数:如何设置
				         	SeekStart   = 0 // seek relative to the origin of the file
					        SeekCurrent = 1 // seek relative to the current offset
					        SeekEnd     = 2 // seek relative to the end

			      断点续传 :
			         文件的传递:本质都是文件的复制
			         复制到当前的工程下
			         思路:边复制，边记录复制的总量
		、
	*/

	//srcFile := "com/lc/go_package_demo/fileDemo/LC_Life_Condiment_Copy.jpg"
	srcFile := "com/lc/go_package_demo/go-gopher.png"
	desFile := srcFile[strings.LastIndex(srcFile, "/")+1:]
	fmt.Println(desFile)

	tempFile := desFile + "_temp.txt" //临时文件
	fmt.Println(tempFile)

	file1, err := os.Open(srcFile)
	utils.HandleErr(err)
	file2, err := os.OpenFile(desFile, os.O_CREATE|os.O_WRONLY, os.ModePerm)
	utils.HandleErr(err)
	file3, err := os.OpenFile(tempFile, os.O_CREATE|os.O_RDWR, os.ModePerm)
	utils.HandleErr(err)

	//关闭文件
	defer file1.Close()
	defer file2.Close()

	//step1:先读取临时文件的数据，再seek
	file3.Seek(0, io.SeekStart)
	bs := make([]byte, 100, 100)
	n1, err := file3.Read(bs)
	//utils.HandleErr(err)
	countStr := string(bs[:n1])
	count, err := strconv.ParseInt(countStr, 10, 64)
	//utils.HandleErr(err)
	fmt.Println(count)

	//step2:设置读写的位置
	file1.Seek(count, io.SeekStart)
	file2.Seek(count, io.SeekStart)
	data := make([]byte, 1024, 1024)

	n2 := -1            //读取的数据量
	n3 := -1            //写出的数据量
	total := int(count) //读取的总量

	//step3:复制文件
	for {
		n2, err = file1.Read(data)
		if err == io.EOF || n2 == 0 {
			fmt.Println("文件复制完毕...", total)
			file3.Close()
			os.Remove(tempFile)
			break
		}
		n3, err = file2.Write(data[:n2])
		total += n3

		//将复制的总量，存储到临时文件
		file3.Seek(0, io.SeekStart)
		file3.WriteString(strconv.Itoa(total))

		fmt.Printf("total:%d\n", total) //输出的是字节

		//假装断电
		//if (total > 8000) {
		//	panic("假装断电了。。。")
		//}
	}
}

//第一次执行 假装断电

/*
输出如下：
【1】com/lc/utils/utils.go 下的 init函数，用于初始化一些信息。。。
【2】另一个 com/lc/utils/utils.go 下的 init函数，用于初始化一些信息。。。
【3】 com/lc/utils/utils2 下的 init函数，用于初始化一些信息。。。
go-gopher.png
go-gopher.png_temp.txt
0
total:1024
total:2048
total:3072
total:4096
total:5120
total:6144
total:7168
total:8192
panic: 假装断电了。。。

goroutine 1 [running]:
main.main()
	D:/all_develop_soft/go1.13.windows-amd64/go_path/src/godemobylc/com/lc/go_package_demo/fileDemo/fileSeekDemoApp2.go:87 +0x8fc
*/

//第二次执行 去掉假装断电 断电续传

/*
输出如下：
【1】com/lc/utils/utils.go 下的 init函数，用于初始化一些信息。。。
【2】另一个 com/lc/utils/utils.go 下的 init函数，用于初始化一些信息。。。
【3】 com/lc/utils/utils2 下的 init函数，用于初始化一些信息。。。
go-gopher.png
go-gopher.png_temp.txt
8192
total:8231
文件复制完毕... 8231

*/
