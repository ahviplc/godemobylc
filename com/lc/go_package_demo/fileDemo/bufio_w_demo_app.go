package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	/*
		     bufio:高效io读写
		         buffer 缓存
		         io: input/output

		     将io包下的Reader,Write对象进行包装，带缓存的包装，提高读写的效率

			func(b *writer) Write(p []byte)(nn int, err error)
			func (b *Writer) WriteByte(c byte)error
			func(b *writer) WriteRune(r rune)(size int, err error)
			func(b *Writer) WriteString(s string)(int, error)

	*/

	fileName := "com/lc/go_package_demo/fileDemo/bufioTxt_w.txt"
	file, err := os.OpenFile(fileName, os.O_CREATE|os.O_WRONLY, os.ModePerm)
	if err != nil {
		fmt.Println("err", err) //err
		return
	}

	defer file.Close()

	//创建Writer对象
	w1 := bufio.NewWriter(file)

	//写数据
	//n, err := w1.WriteString("hello world LC")
	//fmt.Println(err)
	//fmt.Println(n) //14字节
	//w1.Flush() //刷新缓冲区

	for i := 1; i <= 1000; i++ {
		w1.WriteString(fmt.Sprintf("%d:hello", i))
	}
	w1.Flush() //刷新缓冲区

	////Scanner
	////Scanln 如果有空格 则只读取空格之前的
	//s2 := ""
	//fmt.Scanln(&s2)
	//fmt.Println(s2)
	//
	////这种就全部输出 你输入什么 就输出什么
	//b2 := bufio.NewReader(os.Stdin)
	//s2,err := b2.ReadString('\n')
	//fmt.Println(s2)

}
