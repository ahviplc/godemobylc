package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	/*
	     bufio:高效io读写
	         buffer 缓存
	         io: input/output

	     将io包下的Reader,Write对象进行包装，带缓存的包装，提高读写的效率

	   ReadBytes()
	   ReaString()
	   ReadLine()
	*/

	fileName := "com/lc/go_package_demo/fileDemo/bufioTxt.txt"
	file, err := os.Open(fileName)
	if err != nil {
		fmt.Println("err", err) //err
		return
	}

	defer file.Close()

	//创建Reader对象
	b1 := bufio.NewReader(file)

	//1.Read 高效读取
	//p := make([]byte, 1024)
	//n1, err := b1.Read(p)
	//fmt.Println(n1)        //62字节
	//fmt.Println(string(p)) //内容

	//2. ReadLine() 读取一行
	//data,flag,err := b1.ReadLine()
	//fmt.Println(flag)
	//fmt.Println(err)
	//fmt.Println(data)
	//fmt.Println(string(data))

	//2的输出示例:
	//false
	//<nil>
	//[103 111 111 100 32 103 111 111 100 32 115 116 117 100 121]
	//good good study

	//3.ReaString()
	//s1,err := b1.ReadString('\n')
	//fmt.Println(err)
	//fmt.Println(s1)
	//
	//s1,err = b1.ReadString('\n')
	//fmt.Println(err)
	//fmt.Println(s1)
	//
	//s1,err = b1.ReadString('\n')
	//fmt.Println(err)
	//fmt.Println(s1)

	//3的输出示例
	//<nil>
	//good good study
	//
	//<nil>
	//
	//
	//<nil>
	//day day up

	//for循环
	//for{
	//	s1,err := b1.ReadString('\n')
	//	if err == io.EOF{
	//		fmt.Println("读取完毕。。。")
	//		break
	//	}
	//	fmt.Println(s1)
	//}

	//for循环的输出示例
	//good good study
	//
	//
	//
	//day day up
	//
	//
	//
	//By LC
	//
	//
	//
	//读取完毕。。。

	//4.ReadBytes()
	data, err := b1.ReadBytes('\n')
	fmt.Println(err)          //<nil>
	fmt.Println(string(data)) //good good study

	//Scanner
	//Scanln 如果有空格 则只读取空格之前的
	//s2 := ""
	//fmt.Scanln(&s2)
	//fmt.Println(s2)

	//这种就全部输出 你输入什么 就输出什么
	b2 := bufio.NewReader(os.Stdin)
	s2, err := b2.ReadString('\n')
	fmt.Println(s2)

}
