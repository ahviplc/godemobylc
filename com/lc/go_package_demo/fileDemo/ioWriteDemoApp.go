package main

import (
	"fmt"
	"godemobylc/com/lc/utils"
	"os"
)

func main() {
	/*
	 写出数据
	     Reader接口
	     Read(p []byte)(n int, error)
	*/

	//写出数据到本地ioTxtWrite.txt文件中
	//step1:打开文件
	//fileName := "D:/all_develop_soft/go1.13.windows-amd64/go_path/src/godemobylc/com/lc/go_package_demo/fileDemo/ioTxtWrite.txt"
	fileName := "com/lc/go_package_demo/fileDemo/ioTxtWrite.txt"
	//file, err := os.Open(fileName) //打开 只读
	file, err := os.OpenFile(fileName, os.O_CREATE|os.O_WRONLY|os.O_APPEND, os.ModePerm) //os.O_APPEND 代表每次都是在文件末尾追加写入 不加的话 则会被覆盖写入
	if err != nil {
		fmt.Println("err: ", err)
		return
	}
	//step3:关闭文件 使用defer是最后才执行 无论程序执行如何 都会最后执行这个
	defer file.Close()

	//step2:写出数据
	//bs := []byte{65, 66, 67, 68, 69, 70} //ABCDEF
	bs := []byte{97, 98, 99, 100} //abcd
	//n, err := file.Write(bs) //写入所有
	n, err := file.Write(bs[:2]) //切片 写入前两个
	fmt.Println(n)
	utils.HandleErr(err)
	file.WriteString("\n")

	//直接写出字符串
	n, err = file.WriteString("hello LC")
	fmt.Println(n)
	utils.HandleErr(err)

	//字符串转成byte写入
	file.WriteString("\n")
	n, err = file.Write([]byte("today"))
	fmt.Println(n)
	utils.HandleErr(err)
}

//处理err的函数  转移到工具类com/lc/utils/utils.go:22中了
//func HandleErr(err error) {
//	if err != nil {
//		log.Fatal(err)
//	}
//}
