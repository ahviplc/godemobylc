package main

import (
	"fmt"
	"os"
	"path"
	"path/filepath"
)

func main() {

	/*
		  文件操作
		   1：路径:
			   相对路径： reLative
					 lcTxt.txt
					 com/lc/go_package_demo/fileDemo/lcTxt.txt
					相对于当前工程
			   绝对路径： absolute
				D:\all_develop_soft\go1.13.windows-amd64\go_path\src\godemobylc\com\lc\go_package_demo\fileDemo\lcTxt.txt
			   当前目录

			.当前目录
			.. 上一层

		  2.创建文件夹，如果文件夹存在，创建失败
			 os.Mkdir() 创建一层
			 os.MkdirAll() 可以创建多层

		  3.创建文件,Create采用模式0666（任何人都可读写，不可执行）创建一个名为nane的文件，如果文件已存在会截断它（置为空文件
			  os.Create() 创建文件

		  4. 打开文件:让当前的程序，和指定的文件之间建立一个连接
			   os.Open(fileName)
			   os.OpenFile(fileName,mode,perm)  //permission 权限

		  5.关闭文件:程序和文件之间的连接断开
			  file.Close()

		  6.删除文件或目录:谨慎使用 谨慎使用 谨慎使用
			file.Remove() 删除文件和空目录
			file.RemoveAll() 删除所有
	*/

	//1.路径
	fileName1 := "D:/all_develop_soft/go1.13.windows-amd64/go_path/src/godemobylc/com/lc/go_package_demo/fileDemo/lcTxt.txt"
	fileName2 := "lcTxt.txt"
	fmt.Println(filepath.IsAbs(fileName1)) //true 代表是绝对路径
	fmt.Println(filepath.IsAbs(fileName2)) //false 代表不是绝对路径

	fmt.Println(filepath.Abs(fileName1)) //D:\all_develop_soft\go1.13.windows-amd64\go_path\src\godemobylc\com\lc\go_package_demo\fileDemo\lcTxt.txt <nil>
	fmt.Println(filepath.Abs(fileName2)) //D:\all_develop_soft\go1.13.windows-amd64\go_path\src\godemobylc\lcTxt.txt <nil>

	fmt.Println("获取父目录", path.Join(fileName1, "..")) //获取父目录 D:/all_develop_soft/go1.13.windows-amd64/go_path/src/godemobylc/com/lc/go_package_demo/fileDemo

	//创建目录 一层
	//err := os.Mkdir("D:/all_develop_soft/go1.13.windows-amd64/go_path/src/godemobylc/com/lc/go_package_demo/fileDemo/mkdir_diy", os.ModePerm)
	//if err != nil {
	//	fmt.Println("err", err) //再执行一遍,则报错: err mkdir D:/all_develop_soft/go1.13.windows-amd64/go_path/src/godemobylc/com/lc/go_package_demo/fileDemo/mkdir_diy: Cannot create a file when that file already exists.
	//	return
	//}
	//fmt.Println("文件夹创建成功...")

	////创建目录 多层
	//err2 := os.MkdirAll("D:/all_develop_soft/go1.13.windows-amd64/go_path/src/godemobylc/com/lc/go_package_demo/fileDemo/mkdir_diy/mkdir_diy_a/mkdir_diy_b", os.ModePerm)
	//if err2 != nil {
	//	fmt.Println("err2", err2) //用os.Mkdir()执行,则报错: err2 mkdir D:/all_develop_soft/go1.13.windows-amd64/go_path/src/godemobylc/com/lc/go_package_demo/fileDemo/mkdir_diy/mkdir_diy_a/mkdir_diy_b: The system cannot find the path specified.
	//
	//	return
	//}
	//fmt.Println("多层文件夹创建成功...")

	//3.创建文件: Create采用模式0666（任何人都可读写，不可执行）创建一个名为nane的文件，如果文件已存在会截断它（置为空文件）
	file1, err3 := os.Create("D:/all_develop_soft/go1.13.windows-amd64/go_path/src/godemobylc/com/lc/go_package_demo/fileDemo/lcTxt_diy.txt")
	if err3 != nil {
		fmt.Println("err3", err3) //err3
		return
	}
	fmt.Println(file1)

	//向file1中写入
	intCounts, err31 := file1.WriteString("hello LC")
	if err31 != nil {
		fmt.Println("err31", err31) //err31
		return
	}
	fmt.Println(intCounts)

	//fileName3 := "lcTxt_fileName3.txt"
	//file2, err4 := os.Create(fileName3) //创建相对路径的文件，是以当前工程为参照的
	//if err4 != nil {
	//	fmt.Println("err4", err4) //err4
	//	return
	//}
	//fmt.Println(file2)

	//4.打开文件
	//file3,err5 := os.Open(fileName1) //只读的
	//if err5 != nil {
	//	fmt.Println("err5", err5) //err5
	//	return
	//}
	//fmt.Println(file3)

	/*
		   第一个参数:文件名称
		   第二个参数:文件的打开方式
					   const (
					// Exactly one of O_RDONLY, O_WRONLY, or O_RDWR must be specified.
					O_RDONLY int = syscall.O_RDONLY // open the file read-only.
					O_WRONLY int = syscall.O_WRONLY // open the file write-only.
					O_RDWR   int = syscall.O_RDWR   // open the file read-write.
					// The remaining values may be or'ed in to control behavior.
					O_APPEND int = syscall.O_APPEND // append data to the file when writing.
					O_CREATE int = syscall.O_CREAT  // create a new file if none exists.
					O_EXCL   int = syscall.O_EXCL   // used with O_CREATE, file must not exist.
					O_SYNC   int = syscall.O_SYNC   // open for synchronous I/O.
					O_TRUNC  int = syscall.O_TRUNC  // truncate regular writable file when opened.
					)
		   第三个参数:文件的权限:文件不存在创建文件，需要指定权限
	*/
	//file4, err6 := os.OpenFile(fileName1, os.O_RDONLY|os.O_WRONLY, os.ModePerm) //可读 可写
	//if err5 != nil {
	//	fmt.Println("err6", err6) //err6
	//	return
	//}
	//fmt.Println(file4)

	//5.关闭文件
	//file4.Close()

	//6.删除文件或者文件夹
	//删除文件
	//err7 := os.Remove("D:/all_develop_soft/go1.13.windows-amd64/go_path/src/godemobylc/com/lc/go_package_demo/fileDemo/del.txt")
	//if err7 != nil {
	//	fmt.Println("err7", err7) //err7 再执行一次，报错如下: err7 remove D:/all_develop_soft/go1.13.windows-amd64/go_path/src/godemobylc/com/lc/go_package_demo/fileDemo/del.txt: The system cannot find the file specified.
	//	return
	//}
	//fmt.Println("删除文件成功...")

	//删除目录
	//err8 := os.Remove("D:/all_develop_soft/go1.13.windows-amd64/go_path/src/godemobylc/com/lc/go_package_demo/fileDemo/del_dir")
	//if err8 != nil {
	//	fmt.Println("err8", err8) //err8 再执行一次，报错如下: err8 remove D:/all_develop_soft/go1.13.windows-amd64/go_path/src/godemobylc/com/lc/go_package_demo/fileDemo/del_dir: The system cannot find the file specified.
	//	                              // 如下要删除的文件夹下还有文件夹 则报错: err8 remove D:/all_develop_soft/go1.13.windows-amd64/go_path/src/godemobylc/com/lc/go_package_demo/fileDemo/del_dir: The directory is not empty.
	//	return
	//}
	//fmt.Println("删除目录成功...")

	//删除目录 删除所有的目录 不管其下面还有没有其他目录
	//err9 := os.RemoveAll("D:/all_develop_soft/go1.13.windows-amd64/go_path/src/godemobylc/com/lc/go_package_demo/fileDemo/del_dir")
	//if err9 != nil {
	//	fmt.Println("err9", err9) //err9
	//	return
	//}
	//fmt.Println("删除目录成功-无视其目录下还有其他目录...")

}
