package main

import (
	"fmt"
	"io"
	"log"
	"os"
)

func main() {

	/*
		  Seek(offset int64, whence int) (ret int64, err error),设置指针光标的位置
		     第一个参数:偏移量
		     第二个参数:如何设置
		         	SeekStart   = 0 // seek relative to the origin of the file
			        SeekCurrent = 1 // seek relative to the current offset
			        SeekEnd     = 2 // seek relative to the end
	*/

	fileName := "com/lc/go_package_demo/fileDemo/ioTxt.txt" //abcdefghij
	file, err := os.OpenFile(fileName, os.O_RDWR, os.ModePerm)
	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()
	//读写
	bs := []byte{0}
	file.Read(bs)
	fmt.Println(string(bs)) //a

	//使用Seek移动光标
	file.Seek(4, io.SeekStart)
	file.Read(bs)
	fmt.Println(string(bs)) //e

	//使用Seek移动光标
	file.Seek(2, 0) //io.SeekStart
	file.Read(bs)
	fmt.Println(string(bs)) //c

	//使用Seek移动光标
	file.Seek(3, io.SeekCurrent) //io.SeekStart
	file.Read(bs)
	fmt.Println(string(bs)) //g

	file.Seek(0, io.SeekEnd)
	file.WriteString("ABC")
}
