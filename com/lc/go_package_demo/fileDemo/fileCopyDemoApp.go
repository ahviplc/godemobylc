package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
)

func main() {

	/*
	  拷贝文件 txt或者图片等都可以
	*/

	//文本
	//srcFile := "com/lc/go_package_demo/fileDemo/ioTxtWrite.txt"
	//destFile := "com/lc/go_package_demo/fileDemo/ioTxtWrite_Copy.txt"

	//图片
	srcFileImg := "com/lc/go_package_demo/LC_Life_Condiment.jpg"
	//destFileImg := "com/lc/go_package_demo/fileDemo/LC_Life_Condiment_Copy.jpg"

	//CopyFile1
	//total, err := CopyFile1(srcFileImg, destFileImg)
	//fmt.Println(total, err)

	//拷贝完毕。。。
	//54 <nil>

	//CopyFile2
	//destFileImg2 := "com/lc/go_package_demo/fileDemo/LC_Life_Condiment_Copy2.jpg"
	//total2, err2 := CopyFile2(srcFileImg, destFileImg2)
	//fmt.Println(total2, err2)

	//CopyFile3
	destFileImg3 := "com/lc/go_package_demo/fileDemo/LC_Life_Condiment_Copy3.jpg"
	total3, err3 := CopyFile3(srcFileImg, destFileImg3)
	fmt.Println(total3, err3) //15859 <nil>

}

//使用ioutil
func CopyFile3(srcFile, destFile string) (int, error) {
	bs, err := ioutil.ReadFile(srcFile)
	if err != nil {
		return 0, err
	}

	err = ioutil.WriteFile(destFile, bs, 0777)
	if err != nil {
		return 0, err
	}
	return len(bs), nil
}

//调用io的Copy()函数
func CopyFile2(srcFile, destFile string) (int64, error) {
	file1, err := os.Open(srcFile)
	if err != nil {
		return 0, err
	}

	file2, err := os.OpenFile(destFile, os.O_CREATE|os.O_WRONLY|os.O_APPEND, os.ModePerm) //os.O_APPEND 代表每次都是在文件末尾追加写入 不加的话 则会被覆盖写入
	if err != nil {
		return 0, err
	}

	defer file1.Close()
	defer file2.Close()

	return io.Copy(file2, file1)
}

//该函数:用于通过io操作实现文件的拷贝，返回值是拷贝的总数量(字节)，错误
func CopyFile1(srcFile, destFile string) (int, error) {
	file1, err := os.Open(srcFile)
	if err != nil {
		return 0, err
	}

	file2, err := os.OpenFile(destFile, os.O_CREATE|os.O_WRONLY|os.O_APPEND, os.ModePerm) //os.O_APPEND 代表每次都是在文件末尾追加写入 不加的话 则会被覆盖写入
	if err != nil {
		return 0, err
	}

	defer file1.Close()
	defer file2.Close()

	//读写操作
	bs := make([]byte, 1024, 1024)
	n := -1 //读取的数据量
	total := 0
	for {
		n, err = file1.Read(bs)
		if err == io.EOF || n == 0 {
			fmt.Println("拷贝完毕。。。")
			break
		} else if err != nil {
			fmt.Println("报错了。。。")
			return total, err
		}
		total += n
		file2.Write(bs[:n])
	}
	return total, nil
}
