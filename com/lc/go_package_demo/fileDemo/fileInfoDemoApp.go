package main

import (
	"fmt"
	"os"
)

func main() {

	/*
	  FileInfo:文件信息
	      interface
	          Name(),文件名
	          Size(),文件大小 以字节为单位
	*/

	fileInfo, err := os.Stat("com/lc/go_package_demo/fileDemo/lcTxt.txt")
	if err != nil {
		fmt.Println("err", err) //如果故意把文件名字写错 报错信息:err CreateFile com/lc/go_package_demo/fileDemo/lcTxt1.txt: The system cannot find the file specified.
		return
	}
	fmt.Printf("%T\n", fileInfo) //*os.fileStat
	//文件名
	fmt.Println(fileInfo.Name()) //lcTxt.txt
	//文件大小
	fmt.Println(fileInfo.Size()) //0 代表 0字节  如果里面打了个LC 则是2个字节大小
	//是否是目录
	fmt.Println(fileInfo.IsDir()) //IsDirectory false
	//修改时间
	fmt.Println(fileInfo.ModTime()) //2019-09-14 22:12:39.8608232 +0800 CST
	//权限
	fmt.Println(fileInfo.Mode()) //-rw-rw-rw-
}
