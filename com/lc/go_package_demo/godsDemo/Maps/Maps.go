package main

import (
	"fmt"
	"github.com/emirpasic/gods/containers"
)

type Map interface {
	Put(key interface{}, value interface{})
	Get(key interface{}) (value interface{}, found bool)
	Remove(key interface{})
	Keys() []interface{}

	containers.Container
	// Empty() bool
	// Size() int
	// Clear()
	// Values() []interface{}
}

func main() {
	fmt.Println("Maps")
}
