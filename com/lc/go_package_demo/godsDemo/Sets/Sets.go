package main

import (
	"fmt"
	"github.com/emirpasic/gods/containers"
)

type Set interface {
	Add(elements ...interface{})
	Remove(elements ...interface{})
	Contains(elements ...interface{}) bool

	containers.Container
	// Empty() bool
	// Size() int
	// Clear()
	// Values() []interface{}
}

func main() {
	fmt.Println("Set") //Set
}
