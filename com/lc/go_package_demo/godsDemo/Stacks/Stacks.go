package main

import (
	"fmt"
	"github.com/emirpasic/gods/containers"
)

type Stack interface {
	Push(value interface{})
	Pop() (value interface{}, ok bool)
	Peek() (value interface{}, ok bool)

	containers.Container
	// Empty() bool
	// Size() int
	// Clear()
	// Values() []interface{}
}

func main() {
	fmt.Println("Stacks")
}
