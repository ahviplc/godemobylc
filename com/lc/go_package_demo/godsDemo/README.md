emirpasic/gods: GoDS (Go Data Structures). Containers (Sets, Lists, Stacks, Maps, Trees), Sets (HashSet, TreeSet, LinkedHashSet), Lists (ArrayList, SinglyLinkedList, DoublyLinkedList), Stacks (LinkedListStack, ArrayStack), Maps (HashMap, TreeMap, HashBidiMap, TreeBidiMap, LinkedHashMap), Trees (RedBlackTree, AVLTree, BTree, BinaryHeap), Comparators, Iterators, Enumerables, Sort, JSON  

> https://github.com/emirpasic/gods

godsDoc gods的文档 位置:  
> com/lc/go_package_demo/godsDemo/godsDoc/README.md

LC自己整理到了 Iterator   
> com/lc/go_package_demo/godsDemo/godsDoc/README.md:953  
以下的学习相关，请查看文档

数据结构。容器、集合、列表、堆栈、地图、BidiMaps、树、HashSet等!

其他:  
yinggaozhen/awesome-go-cn: 一个很棒的Go框架、库和软件的中文收录大全。 脚本定期与英文文档同步，包含了各工程star数/最近更新时间，助您快速发现优质项目。Awesome Go~  
> https://github.com/yinggaozhen/awesome-go-cn

ahviplc/awesome-go-cn: 一个很棒的Go框架、库和软件的中文收录大全。 脚本定期与英文文档同步，包含了各工程star数/最近更新时间，助您快速发现优质项目。Awesome Go~  
> https://github.com/ahviplc/awesome-go-cn