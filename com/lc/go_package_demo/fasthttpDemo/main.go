package main

import (
	"fmt"
	"github.com/valyala/fasthttp"
)

func GetDemo() {
	// url := `http://httpbin.org/get`
	// lmt api url
	url := "http://114.84.25.5:8081/rest/v1/test" // {"returnCode":"200","returnMessage":"测试接口请求成功！","param":{"a":"a","b":{"1":"1","2":"2","3":"3"},"d":"g","e":"k"}}

	status, resp, err := fasthttp.Get(nil, url)
	if err != nil {
		fmt.Println("请求失败:", err.Error())
		return
	}

	if status != fasthttp.StatusOK {
		fmt.Println("请求没有成功:", status)
		return
	}

	fmt.Println(string(resp))
}

// "Content-Type": "application/x-www-form-urlencoded"
func PostFormDemo() {
	url := `http://httpbin.org/post?key=123`

	// 填充表单，类似于net/url
	args := &fasthttp.Args{}
	args.Add("name", "test")
	args.Add("age", "18")

	status, resp, err := fasthttp.Post(nil, url, args)
	if err != nil {
		fmt.Println("请求失败:", err.Error())
		return
	}

	if status != fasthttp.StatusOK {
		fmt.Println("请求没有成功:", status)
		return
	}

	fmt.Println(string(resp))
}

//  "Content-Type": "application/json",
func PostJsonDemo() {
	url := `http://httpbin.org/post?key=123`

	req := &fasthttp.Request{}
	req.SetRequestURI(url)

	requestBody := []byte(`{"request":"test"}`)
	req.SetBody(requestBody)

	// 默认是application/x-www-form-urlencoded
	req.Header.SetContentType("application/json")
	req.Header.SetMethod("POST")

	resp := &fasthttp.Response{}

	client := &fasthttp.Client{}
	if err := client.Do(req, resp); err != nil {
		fmt.Println("请求失败:", err.Error())
		return
	}

	b := resp.Body()

	fmt.Println("result:\r\n", string(b))
}

// 提供了几个方法：AcquireRequest()、AcquireResponse(),而且也推荐了在有性能要求的代码中，通过 AcquireRequest 和 AcquireResponse 来获取 req 和 resp
func ReqRes() {
	url := `http://httpbin.org/post?key=123`

	req := fasthttp.AcquireRequest()
	defer fasthttp.ReleaseRequest(req) // 用完需要释放资源

	// 默认是application/x-www-form-urlencoded
	req.Header.SetContentType("application/json")
	req.Header.SetMethod("POST")

	req.SetRequestURI(url)

	requestBody := []byte(`{"request":"test"}`)
	req.SetBody(requestBody)

	// 添加header
	req.Header.Add("H1", "h1")

	resp := fasthttp.AcquireResponse()
	defer fasthttp.ReleaseResponse(resp) // 用完需要释放资源

	if err := fasthttp.Do(req, resp); err != nil {
		fmt.Println("请求失败:", err.Error())
		return
	}

	b := resp.Body()

	fmt.Println("result:\r\n", string(b))
}

func main() {
	//PostFormDemo()
	//PostJsonDemo()
	//ReqRes()
	GetDemo()
}

/**
ReqRes()请求返回:
result:
 {
  "args": {
    "key": "123"
  },
  "data": "{\"request\":\"test\"}",
  "files": {},
  "form": {},
  "headers": {
    "Content-Length": "18",
    "Content-Type": "application/json",
	"H1": "h1",
    "Host": "httpbin.org",
    "User-Agent": "fasthttp",
    "X-Amzn-Trace-Id": "Root=1-5f641686-2aba71837937c5a3bf9288cf"
  },
  "json": {
    "request": "test"
  },
  "origin": "114.84.25.5",
  "url": "http://httpbin.org/post?key=123"
}
*/
