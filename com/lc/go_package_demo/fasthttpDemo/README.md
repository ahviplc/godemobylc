golang使用fasthttp 发起http请求 - Go语言中文网 - Golang中文社区  
> https://studygolang.com/articles/17716

GitHub - valyala/fasthttp: Fast HTTP package for Go. Tuned for high performance. Zero memory allocations in hot paths. Up to 10x faster than net/http  
> https://github.com/valyala/fasthttp

安装
> go get -u github.com/valyala/fasthttp

使用了以下库
```markdown
GitHub - andybalholm/brotli: Pure Go Brotli encoder and decoder
https://github.com/andybalholm/brotli

GitHub - klauspost/compress: Optimized compression packages
https://github.com/klauspost/compress

GitHub - valyala/bytebufferpool: Anti-memory-waste byte buffer pool
https://github.com/valyala/bytebufferpool
```