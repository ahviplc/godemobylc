package utilsForEcho

import "fmt"

func USleep(name string) string {
	fmt.Println(name, " 在睡觉...")
	return name + " 在睡觉..."
}

func Drink(name string) string {
	fmt.Println(name, " 在喝水...")
	return name + " 在喝水..."
}
