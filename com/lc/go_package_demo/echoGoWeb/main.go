package main

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"main.go/utilsForEcho"
	"net/http"
)

func main() {
	// Echo instance
	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// Routes
	e.GET("/", hello)
	e.GET("/lc", lc)
	e.GET("/ahviplc", ahviplc)

	// Start server
	e.Logger.Fatal(e.Start(":1323"))
}

// Handler
func hello(c echo.Context) error {
	return c.String(http.StatusOK, "Hello, World!")
}

// Handler
func lc(c echo.Context) error {
	return c.String(http.StatusOK, utilsForEcho.USleep("LC"))
}

// Handler
func ahviplc(c echo.Context) error {
	return c.String(http.StatusOK, utilsForEcho.Drink("ahviplc"))
}
