# go 模块加速

## 中国最可靠的 Go 模块代理-使用七牛云 CDN .
goproxy中国
> https://goproxy.cn/

The most trusted Go module proxy in China. https://goproxy.cn

> https://github.com/goproxy/goproxy.cn

## goproxy用法:

```markdown
用法
Go 1.13 及以上（推荐）

打开你的终端并执行

$ go env -w GO111MODULE=on
$ go env -w GOPROXY=https://goproxy.cn,direct
完成。

macOS 或 Linux

打开你的终端并执行

$ export GO111MODULE=on
$ export GOPROXY=https://goproxy.cn
或者

$ echo "export GO111MODULE=on" >> ~/.profile
$ echo "export GOPROXY=https://goproxy.cn" >> ~/.profile
$ source ~/.profile
完成。

Windows

打开你的 PowerShell 并执行

C:\> $env:GO111MODULE = "on"
C:\> $env:GOPROXY = "https://goproxy.cn"
或者

1. 打开“开始”并搜索“env”
2. 选择“编辑系统环境变量”
3. 点击“环境变量…”按钮
4. 在“<你的用户名> 的用户变量”章节下（上半部分）
5. 点击“新建…”按钮
6. 选择“变量名”输入框并输入“GO111MODULE”
7. 选择“变量值”输入框并输入“on”
8. 点击“确定”按钮
9. 点击“新建…”按钮
10. 选择“变量名”输入框并输入“GOPROXY”
11. 选择“变量值”输入框并输入“https://goproxy.cn”
12. 点击“确定”按钮
完成。

自托管 Go 模块代理
你的代码永远只属于你自己，因此我们向你提供目前世界上最炫酷的自托管 Go 模块代理搭建方案。通过使用 Goproxy 这个极简主义项目，你可以在现有的任意 Web 服务中轻松地加入 Go 模块代理支持，要知道 goproxy.cn 就是基于它搭建的。

创建一个名为 goproxy.go 的文件

package main

import (
	"net/http"
	"os"

	"github.com/goproxy/goproxy"
)

func main() {
	g := goproxy.New()
	g.GoBinEnv = append(
		os.Environ(),
		"GOPROXY=https://goproxy.cn,direct", // 使用 goproxy.cn 作为上游代理
		"GOPRIVATE=git.example.com",         // 解决私有模块的拉取问题（比如可以配置成公司内部的代码源）
	)
	g.ProxiedSUMDBs = []string{"sum.golang.org https://goproxy.cn/sumdb/sum.golang.org"} // 代理默认的校验和数据库
	http.ListenAndServe("localhost:8080", g)
}
并且运行它

$ go run goproxy.go
然后通过把 GOPROXY 设置为 http://localhost:8080 来试用它。另外，我们也建议你把 GO111MODULE 设置为 on。

就这么简单，一个功能完备的 Go 模块代理就搭建成功了。事实上，你可以将 Goproxy 结合着你钟爱的 Web 框架一起使用，比如 Gin 和 Echo，你所需要做的只是多添加一条路由而已。更高级的用法请查看文档。
```

## go.mod书写示例:

```markdown
module main.go

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/stretchr/testify v1.6.1 // indirect
	github.com/valyala/fasttemplate v1.1.1 // indirect
	golang.org/x/crypto v0.0.0-20200604202706-70a84ac30bf9 // indirect
)

replace (
	golang.org/x/net => github.com/golang/net latest
	golang.org/x/tools => github.com/golang/tools latest
	golang.org/x/crypto => github.com/golang/crypto latest
	golang.org/x/sys => github.com/golang/sys latest
	golang.org/x/text => github.com/golang/text latest
	golang.org/x/sync => github.com/golang/sync latest
)
```

## go mod 命令帮助:

```shell script
themacoflc@theDiyPcOfLCdeiMac-Pro echoGoWeb % go help mod    
Go mod provides access to operations on modules.

Note that support for modules is built into all the go commands,
not just 'go mod'. For example, day-to-day adding, removing, upgrading,
and downgrading of dependencies should be done using 'go get'.
See 'go help modules' for an overview of module functionality.

Usage:

        go mod <command> [arguments]

The commands are:

        download    download modules to local cache
        edit        edit go.mod from tools or scripts
        graph       print module requirement graph
        init        initialize new module in current directory
        tidy        add missing and remove unused modules
        vendor      make vendored copy of dependencies
        verify      verify dependencies have expected content
        why         explain why packages or modules are needed

Use "go help mod <command>" for more information about a command.

```

## go mod 命令介绍:
```markdown
go mod 命令介绍
这个子命令用来处理 go.mod 文件，上一小节我们已经见过 go mod init 和 go mod tidy 了。

go mod edit -fmt 格式化 go.mod 文件。
go mod edit -require=path@version 添加依赖或修改依赖版本，这里支持模糊匹配版本号，详情可以看下文 go get 的用法。
go mod tidy 从 go.mod 删除不需要的依赖、新增需要的依赖，这个操作不会改变依赖版本。
go mod vendor 生成 vendor 文件夹。
其他的自行 go help mod 查看。

作者：yesuu
链接：https://www.jianshu.com/p/c5733da150c6
```

## 其他

用 golang 1.11 module 做项目版本管理
> https://www.jianshu.com/p/c5733da150c6

Go mod 最简单的理解和使用
> https://blog.csdn.net/ming2316780/article/details/90370913

框架介绍 - GoFrame开发框架 - 砥砺前行，不忘初心 - 高性能Go Web框架
> https://goframe.org/index

GitHub - gogf/gf: GoFrame is a modular, powerful, high-performance and production-ready application development framework of golang.
> https://github.com/gogf/gf

gf: GF(Go Frame)是一款模块化、高性能、生产级的Go基础开发框架。实现了比较完善的基础设施建设以及开发工具链，提供了常用的基础开发模块，如：缓存、日志、队列、数组、集合、容器、定时器、命令行、内存锁、对象池、配置管理、资源管理、数据校验、数据编码、定时任务、数据库ORM、TCP/UDP组件、进程管理/通信等等。并提供了Web服务开发的系列核心组件，如：Router、Cookie、Session、Middleware、服务注册、模板引擎等等，支持热重启、热更新、域名绑定、TLS/HTTPS、Rewrite等特性。
> https://gitee.com/johng/gf

Go Module - 准备工作 - GoFrame开发框架 - 砥砺前行，不忘初心 - 高性能Go Web框架
> https://goframe.org/prepare/gomodule