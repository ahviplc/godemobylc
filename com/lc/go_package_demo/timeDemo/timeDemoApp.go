package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	/*
		time包：
		1年=365天，day
		1天=24小时，hour
		1小时=60分钟， minute
		1分钟=60秒， second
		1秒钟=1000毫秒， millisecond
		1毫秒=1000微秒， microsecond->us
		1微秒=1000纳秒， nanosecond->ns
		1纳秒=1000皮秒， picosecond->ps
	*/

	//1.获取当前时间
	t1 := time.Now()
	fmt.Printf("%T\n", t1)
	fmt.Println(t1) //2019-09-14 20:59:14.4931922 +0800 CST m=+0.006015601

	//2.获取指定时间
	t2 := time.Date(2016, 9, 9, 9, 9, 9, 0, time.Local)
	fmt.Println(t2) //2016-09-09 09:09:09 +0800 CST

	//3.time-->string之间的转换
	/*
	   t1.Format（"格式模板"）-> string
	   模板的日期必须是固定：06-1-2-3-4-5
	   这个日期貌似是纪念go语言诞生的日子
	*/
	s1 := t1.Format("2006年1月2日 15:04:05")
	fmt.Println(s1) //2019年9月14日 21:07:34

	s2 := t1.Format("2006/01/02")
	fmt.Println(s2) //2019/09/14

	//string-->time
	/*
		time.Parse("模板",str)-> time,err
	*/
	s3 := "1993年02月21日" //string
	t3, err := time.Parse("2006年01月02日", s3)
	if err != nil {
		fmt.Println("err", err)
	}
	fmt.Println(t3)        //1993-02-21 00:00:00 +0000 UTC
	fmt.Printf("%T\n", t3) //time.Time

	fmt.Println(t1.String()) //2019-09-14 21:15:22.6866402 +0800 CST m=+0.007019201

	//4. 根据当前时间，获取指定的内容
	year, month, day := t1.Date() //年月日
	fmt.Println(year, month, day) //2019 September 14

	hour, min, sec := t1.Clock() //时分秒
	fmt.Println(hour, min, sec)  //21 19 36

	year2 := t1.Year()        //年
	fmt.Println("年：", year2)  //年： 2019
	fmt.Println(t1.YearDay()) //257 代表今天过去了多少天

	month2 := t1.Month()
	fmt.Println("月:", month2)
	fmt.Println("日", t1.Day())
	fmt.Println("时:", t1.Hour())
	fmt.Println("分钟:", t1.Minute())
	fmt.Println("秒:", t1.Second())
	fmt.Println("纳秒:", t1.Nanosecond())
	//月: September
	//	日 14
	//时: 21
	//分钟: 26
	//秒: 22
	//纳秒: 670669000

	fmt.Println(t1.Weekday()) //星期 Saturday

	//5.时间题：指定的日期，距离1970年1月1日中0时0分0秒的时间差值：秒，纳秒
	t4 := time.Date(1970, 1, 1, 1, 0, 0, 0, time.UTC)
	timeStamp1 := t4.Unix() //秒的差值
	fmt.Println(timeStamp1) // 3600秒

	timeStamp2 := t1.Unix() //秒的差值
	fmt.Println(timeStamp2) //距离当前时间【2019-09-14 21:33:54.3631691 +0800 CST m=+0.006016301】 1568468034

	timeStamp3 := t4.UnixNano() //纳秒的差值
	fmt.Println(timeStamp3)     // 3600秒 = 3600 000 000 000 纳秒  【3600000000000】

	timeStamp4 := t1.UnixNano() //纳秒的差值
	fmt.Println(timeStamp4)     // 当前时间的纳秒时间戳【2019-09-14 21:37:00.1434167 +0800 CST m=+0.008024301】 1568468220秒 / 1568468220 143 416 700纳秒  【1568468220143416700】

	//6.时间间隔
	t5 := t1.Add(time.Minute)
	fmt.Println(t1)
	fmt.Println(t5)
	fmt.Println(t1.Add(24 * time.Hour))
	//2019-09-14 21:40:41.0883322 +0800 CST m=+0.008019901
	//2019-09-14 21:41:41.0883322 +0800 CST m=+60.008019901
	//2019-09-15 21:40:41.0883322 +0800 CST m=+86400.008019901

	t6 := t1.AddDate(1, 0, 0)
	fmt.Println(t6) //2020-09-14 21:41:44.3153752 +0800 CST

	d1 := t5.Sub(t1)
	fmt.Println(d1) //1m0s 相差1分钟

	//7.睡眠
	time.Sleep(3 * time.Second) //让当前的程序进入睡眠状态 睡眠3秒
	fmt.Println("main...over......")

	//睡眠【1-10】的随机秒数
	rand.Seed(time.Now().UnixNano())
	randNum := rand.Intn(10) + 1 //int 随机0-9  加上1 就是1-10
	fmt.Println(randNum)
	time.Sleep(time.Duration(randNum) * time.Second)
	fmt.Println("睡醒了...")

	fmt.Println("that is all")
}
