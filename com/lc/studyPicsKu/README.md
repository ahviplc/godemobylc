最常使用的代码收集整理:

```go

输出语句:
fmt.Println()

err错误判断：
if err != nil {
		fmt.Println("err", err) //err
		return
	}

输出类型:
fmt.Printf("%T\n", fileInfo)

//处理err的函数
func HandleErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

```

2019Golang进阶实战编程(学完基础必看)【GO语言】_哔哩哔哩 (゜-゜)つロ 干杯~-bilibili  
> https://www.bilibili.com/video/av59700019/?p=1

备注：p1-p11已看完 感觉非常好  
时间：2019年9月15日14:59:36