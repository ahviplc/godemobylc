package pkg

import (
	"fmt"
	"godemobylc/com/lc/utils"
	"godemobylc/com/lc/utils/uuidutils"
)

func MyTest() {
	fmt.Println(" i am from com/lc/pkg/test.go begin------------------------------------------")
	utils.UtilsApp()
	uuidutils.UuidApp()
	fmt.Println(" i am from com/lc/pkg/test.go end------------------------------------------")
}

func init() {
	fmt.Println("【4】 com/lc/pkg/test.go 下的 init函数，用于初始化一些信息。。。")
}
